class Edition {

    private String nom;
    private Localite localite;

    public Edition(String nom, Localite localite){
        this.localite = localite;
        this.nom = nom;
    }
    
    @Override
    public String toString(){
        return "les editeurs : "+nom;
    }
    
}
