public class Ouvrage {

    private int anneeEdition;
    private Edition editeur;
    private String titre;

    public Ouvrage(int anneeEdition, String titre, Edition editeur){
        this.anneeEdition = anneeEdition;
        this.editeur = editeur;
        this.titre = titre;
    }

    public String getTitre(){
        return titre;
    }
    public String categorie(){
        return "L'ouvrage";
    }
    public String toString(){
        return categorie()+" edite en "+anneeEdition+", par "+editeur;
    }
}
