public class Main {

    public static void main(String[] args){
        Localite localite = new Localite("Namur", 345000);
        Edition edition = new Edition("Donny Mboma", localite);
        Ouvrage ouvrage = new Ouvrage(2020,"Java how to program",edition);
        Livre livre = new Livre(2020, "Java", edition, 300, 2, 22);
        System.out.println(ouvrage);
        System.out.println("-------------------------------------");
        System.out.println(livre);
        //System.out.println(livre.categorie());
    }
    
}
