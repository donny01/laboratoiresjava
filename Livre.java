public class Livre extends Ouvrage implements Publication {
    
    private int nbPagesTableMatieres;
    private int nbPagesPreface;
    private int nbPagesContenu;

    public Livre(int anneeEdition, String titre, Edition editeur, int nbPagesContenu, int nbPagesPreface, int nbPagesTableMatieres){
        super(anneeEdition, titre, editeur);
        this.nbPagesContenu = nbPagesContenu;
        this.nbPagesPreface = nbPagesPreface;
        this.nbPagesTableMatieres = nbPagesTableMatieres;
    }
    public int totalPages(){
        return this.nbPagesContenu + this.nbPagesPreface + this.nbPagesTableMatieres;
    }
    @Override
    public String categorie(){
        return "le livre";
    }
    @Override
    public String toString(){
        return super.toString()+"\n"+
        "Compte : "+totalPages()+" pages, et est intitulé "+getTitre();
    }
}
